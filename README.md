# QR Code Reader

The project provides an example of using video filters to process QR codes.

The main purpose is to show not only what features are available to work with these API,
but also how to use them correctly.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/QrCodeReader/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/QrCodeReader/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/QrCodeReader/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/QrCodeReader/-/commits/dev)

### Supported data types in QR codes

This application supports such types of data as: email address, link, phone number, SMS message,
contact, geolocation, calendar event and plain text.

Pre-generated QR codes for testing are located in the [test-qr-codes](test-qr-codes) directory.
A website was used to generate them https://qrcode.tec-it.com/ru/Raw/.

### Limitations

Generating QR-codes for texts in Russian is not supported.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.QrCodeReader.pro](ru.auroraos.QrCodeReader.pro)** file describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[images](qml/images)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[QrCodeReader.qml](qml/QrCodeReader.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.QrCodeReader.spec](rpm/ru.auroraos.QrCodeReader.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[handler](src/handler)** directory contains QR codes handler.
  * **[types](src/types)** directory contains list types QR codes.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[test-qr-codes](test-qr-codes)** directory contains examples QR codes.
* **[ru.auroraos.QrCodeReader.desktop](ru.auroraos.QrCodeReader.desktop)** file defines the display and parameters for launching the application.

## Compatibility

The project is compatible with the fifths version of the Aurora OS.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
