<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;В проекте приведен пример использования видеофильтров для обработки QR-кодов.&lt;/p&gt;
                            &lt;p&gt;Основная цель - показать не только, какие функции доступны для
                            работы с этими API, но и как их правильно использовать.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>The 3-Clause BSD License</source>
        <translation>The 3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2024 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>CreateQRCodePage</name>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="152"/>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="69"/>
        <source>QR-code resolution:</source>
        <translation>Разрешение QR-кода:</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="44"/>
        <source>Create QR-code</source>
        <translation>Создать QR-код</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="86"/>
        <source>Backgound color:</source>
        <translation>Цвет фона:</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="115"/>
        <source>Main color:</source>
        <translation>Основной цвет:</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="144"/>
        <source>Text to encode</source>
        <translation>Текст для кодирования</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateQRCodePage.qml" line="162"/>
        <source>Path to saved QR-code:</source>
        <translation>Путь к сохраннённому QR-коду</translation>
    </message>
</context>
<context>
    <name>MapPage</name>
    <message>
        <location filename="../qml/pages/MapPage.qml" line="44"/>
        <source>Map Viewer</source>
        <translation>Отображение карты</translation>
    </message>
</context>
<context>
    <name>ProcessingPage</name>
    <message>
        <location filename="../qml/pages/ProcessingPage.qml" line="67"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../qml/pages/ProcessingPage.qml" line="19"/>
        <source>Execute</source>
        <translation>Выполнить</translation>
    </message>
    <message>
        <location filename="../qml/pages/ProcessingPage.qml" line="84"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/ProcessingPage.qml" line="22"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../qml/pages/ProcessingPage.qml" line="74"/>
        <source>Source text</source>
        <translation>Исходный текст</translation>
    </message>
    <message>
        <location filename="../qml/pages/ProcessingPage.qml" line="90"/>
        <source>Formated text</source>
        <translation>Отформатированный текст</translation>
    </message>
</context>
<context>
    <name>QrCodeReader</name>
    <message>
        <location filename="../qml/QrCodeReader.qml" line="10"/>
        <source>QR Code Reader</source>
        <translation>Сканер QR‑кодов</translation>
    </message>
</context>
<context>
    <name>RecognitionPage</name>
    <message>
        <location filename="../qml/pages/RecognitionPage.qml" line="27"/>
        <source>Create a QR-code</source>
        <translation>Создать QR‑код</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecognitionPage.qml" line="36"/>
        <source>About</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecognitionPage.qml" line="124"/>
        <source>Processing</source>
        <translation>Обработка</translation>
    </message>
</context>
</TS>
