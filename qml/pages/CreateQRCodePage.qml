// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.QrCodeReader 1.0
import Amber.QrFilter 1.0

Page {
    property int imageMargin: 15
    property color chosenBackgroundColor: "#FFFFFF"
    property color chosenMainColor: "#000000"
    property var colors: ["#000000", "#FFFFFF", "#E4E4DA", "#F5F3D7", "#D3F2EA", "#87C0EA", "#F5C189", "#E2F99A", "#FFFF99", "#D9BB7B", "#68C3E2", "#9C9291", "#CDA4DE", "#EE9DC3", "#FFFF00", "#469BC3", "#F49B00", "#95B90B", "#00CC00", "#AA7D55", "#059D9E", "#478CC6", "#D67240", "#8D7452", "#FF6600", "#5F8265", "#5E748C", "#A06EB9", "#009900", "#DE378B", "#FF0000", "#A83D15", "#4C5156", "#990066", "#0000FF", "#80081B", "#5B1C0C", "#002541", "#003300", "#2C1577", "#300F06"]

    function createQRCode() {
        qrCode.width = parseInt(sizeCombobox.value);
        qrCode.height = parseInt(sizeCombobox.value);
        qrCode.margin = imageMargin;
        qrCode.text = textToEncode.text;
        qrCode.update();
    }

    CreateQrCodePageController {
        id: controller

        onImageSaved: {
            imagePathLabel.text = imagePath;
            outputHint.visible = true;
            result.source = image;
        }
    }

    QrCode {
        id: qrCode

        onGenerationFinished: {
            controller.saveQrCodeToFile(qrCode.image, chosenMainColor, chosenBackgroundColor);
        }
    }

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: qsTr("Create QR-code")
    }

    SilicaFlickable {
        objectName: "flickable"

        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            bottomMargin: Theme.horizontalPageMargin
        }
        contentHeight: column.height

        Column {
            id: column

            objectName: "column"
            width: parent.width
            spacing: Theme.paddingLarge

            ComboBox {
                id: sizeCombobox

                label: qsTr("QR-code resolution:")
                menu: ContextMenu {
                    MenuItem {
                        text: "200"
                    }
                    MenuItem {
                        text: "400"
                    }
                    MenuItem {
                        text: "600"
                    }
                }
            }

            ValueButton {
                id: backgroundColorValueButton

                label: qsTr("Backgound color:")
                value: chosenBackgroundColor

                onClicked: {
                    var page = pageStack.push("Sailfish.Silica.ColorPickerPage", {
                            "colors": colors
                        });
                    page.colorClicked.connect(function (color) {
                            chosenBackgroundColor = color;
                            pageStack.pop();
                        });
                }

                Rectangle {
                    anchors {
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    width: height
                    height: Theme.itemSizeSmall
                    color: chosenBackgroundColor
                    radius: 5
                    visible: chosenBackgroundColor.length !== 0
                }
            }

            ValueButton {
                id: mainColorValueButton

                label: qsTr("Main color:")
                value: chosenMainColor

                onClicked: {
                    var page = pageStack.push("Sailfish.Silica.ColorPickerPage", {
                            "colors": colors
                        });
                    page.colorClicked.connect(function (color) {
                            chosenMainColor = color;
                            pageStack.pop();
                        });
                }

                Rectangle {
                    anchors {
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    width: height
                    height: Theme.itemSizeSmall
                    color: chosenMainColor
                    radius: 5
                    visible: chosenMainColor.length !== 0
                }
            }

            TextField {
                id: textToEncode

                placeholderText: qsTr("Text to encode")
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Button {
                id: encodeBtn

                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Create")
                onClicked: {
                    createQRCode();
                }
                enabled: textToEncode.length > 0
            }

            Label {
                id: outputHint

                text: qsTr("Path to saved QR-code:")
                color: "darkGray"
                visible: false
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                id: imagePathLabel

                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                anchors {
                    left: parent.left
                    leftMargin: 1.5 * Theme.paddingLarge
                    right: parent.right
                    rightMargin: 1.5 * Theme.paddingLarge
                }
            }

            Image {
                id: result

                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
}
