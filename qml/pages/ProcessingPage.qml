// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import QtPositioning 5.2
import Sailfish.Silica 1.0
import ru.auroraos.QrCodeReader 1.0

Dialog {
    objectName: "processingPage"

    property alias qrCodeData: qrCodeHandler.sourceText

    function acceptIsAvail(handler) {
        return handler.executable || handler.type == QRCodeType.Geo;
    }

    function acceptBtnText(handler) {
        if (handler.executable) {
            return qsTr("Execute");
        }
        if (handler.type == QRCodeType.Geo) {
            return qsTr("Show");
        }
        return "";
    }

    function actionOnAccept(handler) {
        if (handler.executable) {
            handler.execute();
        }
        if (handler.type == QRCodeType.Geo) {
            pageStack.push(Qt.resolvedUrl("MapPage.qml"), {
                    "mapPosition": QtPositioning.coordinate(handler.fields["latitude"], handler.fields["longitude"]),
                    "mapZoomLevel": 19
                });
        }
    }

    forwardNavigation: acceptIsAvail(qrCodeHandler)

    onAccepted: actionOnAccept(qrCodeHandler)

    QRCodeHandler {
        id: qrCodeHandler

        objectName: "qrCodeHandler"
    }

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: column

            objectName: "column"
            width: parent.width
            spacing: Theme.paddingLarge

            DialogHeader {
                objectName: "pageHeader"
                cancelText: qsTr("Back")
                acceptText: acceptBtnText(qrCodeHandler)
                acceptTextVisible: acceptIsAvail(qrCodeHandler)
            }

            SectionHeader {
                objectName: "sourceTextSection"
                text: qsTr("Source text")
            }

            TextArea {
                objectName: "sourceText"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: implicitHeight
                readOnly: true
                text: qrCodeHandler.sourceText
            }

            SectionHeader {
                objectName: "formatedTextSection"
                text: qsTr("Formated text")
            }

            TextArea {
                objectName: "formatedText"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: implicitHeight
                readOnly: true
                text: qrCodeHandler.formatedText
            }
        }
    }
}
