// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import QtLocation 5.0
import QtPositioning 5.2
import Sailfish.Silica 1.0

Page {
    objectName: "mapPage"

    property var mapPosition
    property int mapZoomLevel

    Plugin {
        id: mapPlugin

        objectName: "mapPlugin"
        name: "webtiles"
        allowExperimental: false

        PluginParameter {
            objectName: "schemeParameter"
            name: "webtiles.scheme"
            value: "https"
        }

        PluginParameter {
            objectName: "hostParameter"
            name: "webtiles.host"
            value: "tile.openstreetmap.org"
        }

        PluginParameter {
            objectName: "pathParameter"
            name: "webtiles.path"
            value: "/${z}/${x}/${y}.png"
        }
    }

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: qsTr("Map Viewer")
    }

    Map {
        objectName: "map"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        plugin: mapPlugin

        Component.onCompleted: {
            center = QtPositioning.coordinate(mapPosition.latitude, mapPosition.longitude);
            zoomLevel = mapZoomLevel;
        }

        MapQuickItem {
            objectName: "mapItem"
            coordinate: QtPositioning.coordinate(mapPosition.latitude, mapPosition.longitude)
            anchorPoint.x: image.width / 2
            anchorPoint.y: image.height
            sourceItem: Image {
                id: image

                width: 64
                height: 64
                source: "image://theme/icon-m-whereami?darkRed"
            }
        }
    }
}
